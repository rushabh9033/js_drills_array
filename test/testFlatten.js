const flatten = require("../flatten.js");

const nestedArray = [1, [2], [[3]], [[[4]]]];

const result = flatten(nestedArray);

console.log(result);

const originalFlattenResult = nestedArray.flat(nestedArray.length);

let allResultMatch = true;

for (let i = 0; i < originalFlattenResult.length; i++) {
  const myvalue = result[i];
  const originalFlattenValue = originalFlattenResult[i];
  if (myvalue !== originalFlattenValue) {
    allResultMatch = false;
    break;
  }
}

if (allResultMatch) {
  console.log("Results Matched");
} else {
  console.log("results did not matched");
}
