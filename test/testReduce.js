const reduce = require("../reduce.js");

const items = [1, 2, 3, 4, 5, 5];

const sumCallBack = (initialValue, currentValue) => initialValue + currentValue;

let result = reduce(items, sumCallBack, 0);

console.log(result);

const originalReduceResult = items.reduce(sumCallBack, 0);

let allResultMatch = true;

for (let i = 0; i < originalReduceResult.length; i++) {
  const myvalue = result[i];
  const originalReduceValue = originalReduceResult[i];
  if (myvalue !== originalReduceValue) {
    allResultMatch = false;
    break;
  }
}

if (allResultMatch) {
  console.log("Results Matched");
} else {
  console.log("results did not matched");
}
