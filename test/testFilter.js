const filter = require("../filter.js");

const items = [1, 2, 3, 4, 5, 5];

const callBack = (element) => {
  return element <= 5;
};

let result = filter(items, callBack);

console.log(result);

const originalFilterResult = items.filter(callBack);

let allResultMatch = true;

for (let i = 0; i < originalFilterResult.length; i++) {
  const myvalue = result[i];
  const originalFilterValue = originalFilterResult[i];
  if (myvalue !== originalFilterValue) {
    allResultMatch = false;
    break;
  }
}

if (allResultMatch) {
  console.log("Results Matched");
} else {
  console.log("results did not matched");
}
