const map = require("../map.js");

const items = [1, 2, 3, 4, 5, 5];

const callBack = (element) => element;
const result = map(items, callBack);
console.log(result);

const originalMapResult = items.map(callBack);

let allElementMatch = true;
for (let i = 0; i < originalMapResult.length; i++) {
  const myValue = result[i];
  const originalMapValue = originalMapResult[i];

  if (myValue !== originalMapValue) {
    allElementMatch = false;
    break;
  }
}

if (allElementMatch) {
  console.log("Results are match.");
} else {
  console.log("Result did not match.");
}
