function flatten(elements) {
    let flat_arr = []
    for (let i = 0; i < elements.length; i++) {
        if (Array.isArray(elements[i])) {
            let flat = flatten(elements[i])
            flat_arr = flat_arr.concat(flat)

        } else {
            flat_arr.push(elements[i])
        }
    }
    return flat_arr;
}

module.exports = flatten;  