function filter(elements, cb) {
    const filterArray = []

    for (let i = 0; i < elements.length; i++) {
        let res = cb(elements[i])
        if (res) {
            filterArray.push(elements[i])
        }
    }

    return filterArray;
}

module.exports = filter;