const items = [1, 2, 3, 4, 5, 5];

function reduce(elements, cb, startingValue) {
  let total = startingValue;
  for (let i = 0; i < elements.length; i++) {
    let current = elements[i];
    total = cb(total, current);
  }
  return total;
}

module.exports = reduce;
