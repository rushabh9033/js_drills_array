const items = [1, 2, 3, 4, 5, 5];
function map(elements, cb) {
  const arr = [];
  for (let i = 0; i < elements.length; i++) {
    let arrElement = cb(elements[i]);
    arr.push(arrElement);
  }
  return arr;
}
module.exports = map;
