function find(elements, cb) {
  for (let i = 0; i < elements.length; i++) {
    let results = cb(elements[i]);
    if (results) {
      return elements[i];
    }
  }
}

module.exports = find;
